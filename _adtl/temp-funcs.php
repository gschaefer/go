<?php
	function dskLastModTimestamp() {echo time();}
	function DSKdotMinCheck($filename) {
		$modding = isset($_GET['modding']);
		if($modding) {
			$filename = str_replace(".min.css", ".css", $filename);
			$filename = str_replace(".min.js", ".js", $filename);				
		}
		return $filename;
	}
	
	function dsk_wp_is_mobile() {
		return false;
	}
	function mobOrDsktpInitialClass($mobClass, $dsktpClass) {
		if(dsk_wp_is_mobile()) {return $mobClass;}
		else {return $dsktpClass;}
	}
	function web_crumbs() {return false;}
	function shareThisWrapper() {return false;}
	function is_page($idsToTest = 0) {
		if(isset($_GET['id'])) {$actualID = $_GET['id'];}
		else {$actualID = 99999999;}
		if($idsToTest == 0) {
			return true;
		} elseif(is_array($idsToTest)) {
			if(in_array($actualID, $idsToTest)) {return true;}
		} else {
			if($actualID == $idsToTest) {return true;}
			else {return false;}
		}
	}
	function define_temporary_theme_directory($obsoleteDir) {
		$theDir = dirname(__FILE__);
		$thePos = strripos($theDir, DIRECTORY_SEPARATOR . "wp-content" . DIRECTORY_SEPARATOR);
		$theDir = substr($theDir, $thePos);
		$thePos = stripos($theDir, $obsoleteDir);
		$theDirLeft = substr($theDir, 0, $thePos - 1);
		$obsoleteDirStringLength = strlen($obsoleteDir);
		$theDirRight = substr($theDir, $thePos + $obsoleteDirStringLength);
		$finalDir = $theDirLeft . $theDirRight;
		define("TEMPORARYTHEMEDIRECTORY", $finalDir);
	}
	define_temporary_theme_directory("_adtl");
	function curDir() {return TEMPORARYTHEMEDIRECTORY;}
	if(isset($_GET['minifyall'])) {
		header("location: " . get_stylesheet_directory_uri() . "/css/minifier.php");
	}
	function is_front_page() {
		if(isset($_GET['sub']) || (isset($_GET['page']) && is_numeric($_GET['page']))) {return false;}
		else {return true;}
	}
	function get_stylesheet_directory_uri() {return curDir();}
	function body_class($adtlClass = "") {
		if(is_front_page()) {$classes = "home";}
		elseif(isset($_GET['page']) && is_numeric($_GET['page'])) {
			$classes = "page-id-" . $_GET['page'];
		}
		else {$classes = "";}
		if($adtlClass !== "") {$classes .= " " . $adtlClass;}
		echo "class='$classes'";
	}
	function copyrightYears($startYear) {
		$curYear = date("Y");
		if( $curYear > $startYear ) {echo $startYear . ' - ' . $curYear;}
		else {echo $curYear;}
	}
	function get_header() {include "header.php";}
	function get_sidebar() {include "sidebar.php";}
	function get_footer() {include "footer.php";}
	function wp_title() {echo "A NextClient Website";}
	function wp_head() {};
	function wp_footer() { ?>
		<script>
		(function () {
			/* Width Indicator */
			var wiTO, jqCheckTimeoutLapse = 100;
			function addWidthIndicator($) {		
				var rBody = $('body');
				rBody.append('<div id="widthIndicator" />');
				var widthIndicator = $('#widthIndicator')
				widthIndicator.css({'position': 'fixed', 'padding' : '20px', 'left' : '45%', 'top' : '45%', 'background' : '#8D3828', 'color' : '#fff', 'z-index' : '999', 'border-radius' : '5px', 'opacity' : '0.8'});
				function indicateWidth() {widthIndicator.html(rBody.width());}
				indicateWidth();
				$(window).resize(function(){indicateWidth();});
			}
			function runjQueryStuff() {
				addWidthIndicator(jQuery);					
			}
			function testForjQuery() {
				clearTimeout(wiTO);
				if(typeof jQuery != "undefined") {
					runjQueryStuff();
				} else {
					wiTO = setTimeout(function(){testForjQuery();}, jqCheckTimeoutLapse);
				}
			}
			testForjQuery();			
			/* ends w_i */ 
		}());
		</script><?php
	};
	function bloginfo($item) {
		if($item == "stylesheet_url") {echo curDir() . "/style.css";}
		elseif($item == "template_url") {echo "../universal";}
		elseif($item == "pingback_url") {echo "pingback_url_goes_here";}
	}
	function wp_nav_menu($prefs) {
		$unwrappedMain = <<<EOT
	  <li><a href="#">Item 1</a></li>
	  <li class="menu-item-has-children"><a href="#">Item 2</a>
		  <ul>
			<li><a href="#">Item 2.1</a></li>
			<li><a href="#">Item 2.2</a>
				<ul>
					<li><a href="#">Item 2.2a</a></li>
					<li><a href="#">Item 2.2b</a>
						<ul>
							<li><a href="#">Item 2.2b1</a></li>
							<li><a href="#">Item 2.2b2</a>
								<ul>
									<li><a href="#">Item 2.2b2a</a></li>
									<li><a href="#">Item 2.2b2b</a></li>
								</ul>
							</li>
							<li><a href="#">Item 2.2b3</a></li>
						</ul>
					</li>
					<li><a href="#">Item 2.2c</a></li>
				</ul>
			</li>
			<li><a href="#">Item 2.3</a></li>
		  </ul>
	  </li>
	  <li class="menu-item-has-children"><a href="#">Item 3</a>
		  <ul>
			<li><a href="#">Item 3.1</a></li>
			<li><a href="#">Item 3.2</a></li>
			<li><a href="#">Item 3.3</a></li>
		  </ul>
	  </li>
	  <li><a href="#">Item 4</a></li>
EOT;
		$unwrappedOther = <<<EOT
		<li><a href="#">Home</a></li>
		<li><a href="#">Site Map</a></li>
		<li><a href="#">Disclaiemr</a></li>
		<li><a href="#">Contact</a></li>
EOT;
		if($prefs['theme_location'] == "main-nav") {$partialMenu = $unwrappedMain;}
		else {$partialMenu = $unwrappedOther;}
		$menu = $prefs['items_wrap'];
		$menu = str_replace("%3\$s", $partialMenu, $menu);
		echo $menu;
	}
	function get_queried_object_id() {}
	function nextclient_form($the_form_ID) {echo "[FORM_{$the_form_ID}]";}
	function get_stylesheet_directory() {return getcwd();}