<?php

// START Register Navigations
register_nav_menus (
	array (
		'main-nav' => 'Main',
		'footer-nav' => 'Footer'
	)
);
// END Register Navigations

//Uncomment line below and set it to desired tag line for the main blog page:
//$nc_blog_home_tagline = 'Recent Blog Posts';

//Uncomment line below to allow automatic feeds - when client requests them
//add_theme_support( 'automatic-feed-links' );

add_filter ('jetpack_implode_frontend_css', '__return_false');
add_filter( 'jetpack_enable_open_graph', '__return_false' );
