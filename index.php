<?php $isFrontPage = true; include '_adtl/temp-funcs.php'; ?>
<?php if(session_id() == '') {session_start();} ?>
<!DOCTYPE HTML>
<!--[if lt IE 7 ]>				<html lang="en" class="ie6 ltie9"> <![endif]-->
<!--[if IE 7 ]>					<html lang="en" class="ie7 ltie9"> <![endif]-->
<!--[if IE 8 ]>					<html lang="en" class="ie8 ltie9"> <![endif]-->
<!--[if IE 9 ]>					<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->	<html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta id="theViewport" name="viewport" content="width=device-width, initial-scale=1.0">
	<script>
	!function(){function t(t){function n(){var n;n=90===window.orientation||-90===window.orientation?t[0]:t[1],theViewport.setAttribute("content","width="+n)}theViewport=document.getElementById("theViewport"),n(),window.addEventListener("orientationchange",n)}void 0!==window.orientation&&1024===screen.height&&t([1100,1100])}();</script>
	<?php /* mod last digits on the script above to landscape, portrait min-width values */ ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/<?php echo DSKdotMinCheck('style.css'); ?>" />
	<!--[if lte IE 8]>
	<script src="https://d78c52a599aaa8c95ebc-9d8e71b4cb418bfe1b178f82d9996947.ssl.cf1.rackcdn.com/html5shiv/html5shiv.3.7.3.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico" type="image/vnd.microsoft.icon">
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico" />
	<?php wp_head(); ?>
</head>
<body <?php body_class('mob_menu'); ?>>
<div id="wrapper">
<a id="closeMenu" class="menuToggle" href="#">Close Menu</a>
<div id="menuWall" class="menuToggle"></div>
<div id="mmiWrapper"><ul id="mobMenuItems">
	<li id="mm1" class="menuToggle mmi"><a href="#">Menu</a></li>
	<?php /* Format for href value below: tel:+18888888888 */ ?>
	<li id="mm2" class="mmi"><a href="#">Call</a></li>
	<li id="mm3" class="mmi"><a href="#">Email</a></li>
	<li id="mm4" class="mmi"><a href="#">Visit</a></li>
	<li id="mm5" class="mmi"><a class="menuToggle" href="#">Search</a></li>
</ul></div>
<div id="persistentHeader">
<div id="mhWrapper"><header id="mainHeader">
	<a id="logo-main" href="/"><img src="img/logos/main.png" alt="" /></a>
</header></div>
<nav id="mainMenu" class="mm_mob">
	<div id="mblSearchCont">
		<form id="mblSearch" method="get" action="/">
			<input type="text" id="mblsInput" name="s" value="Enter Search Terms Here" />
			<input type="submit" id="mblsSubmit" value="Submit" />
		</form>			
	</div>
	<ul>
	<?php wp_nav_menu(array('theme_location' => 'main-nav', 'container' => false, 'items_wrap' => '%3$s')); ?>
	</ul>
</nav>
</div><!-- persistentHeader -->
<?php if(is_front_page()) { ?>
<div id="homeslides">
    <div id="homeslide1" class="homeslide">
        <div class="slideContent">
            <div class="slideBox">
                <span class="slideHead">xxxxx</span>
                <p class="slideBody">xxxxxx</p>
                <a class="slideMore" href="#">xxxxx</a>
            </div>
        </div>
    </div>
</div>
<?php } else { ?>
<div id="subslide<?=rand(1, 1)?>" class="subslide"></div>
<?php } ?>
<div id="contentArea">
	<article id="mainContent" class="article">
			<?php /* Remove before uploading */
			include('_adtl/content.php'); ?>

		<?php if(is_page()) { ?>
		<div id="shareThisPage">
			<span id="shareThisPageHead">Share This Page:</span>
			<?php echo shareThisWrapper() ?>
		</div>
		<?php } ?>
	</article>
	<div id="sidebar" class="sidebar">
		<h2>Sidebar h2</h2>
		<ul>
			<li>Sidebar List Item</li>
			<li>Sidebar List Item</li>
			<li>Sidebar List Item</li>
			<li>Sidebar List Item</li>
			<li>Sidebar List Item</li>
		</ul>
	</div><!-- sidebar -->
</div><!-- contentArea -->








<!-- BEGIN COPY -->
<div class="sxn-team">
	<div class="teamWrap">
		<span class="teamTitle">Meet Our Dedicated Team</span>
		<ul class="teamList">
			<li class="teamMem teamMem1"><a href="">Eric Less Bensen, Esq.</a></li>
			<li class="teamMem teamMem2"><a href="">Kenneth J. Cotter, Esq.</a></li>
			<li class="teamMem teamMem3"><a href="">Laura A. Moffett, Esq.</a></li>
			<li class="teamMem teamMem4"><a href="">Lorna M. Truett, Esq.</a></li>
			<li class="teamMem teamMem5"><a href="">Cynthia M. Winter, Esq.</a></li>
			<li class="teamMem teamMem6"><a href="">Michael J. Vaghaiwalla, Esq.</a></li>
		</ul>
		<div class="memBox">
			<div class="memInfo">
				<span class="memName">Eric Lee Bensen, Esq.</span>
				<div class="memBody">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.</p>
				</div>
				<a class="memMore" href="#">View Attorney Profile</a>
			</div>
		</div>
	</div>
</div> <!-- end sxn-team -->
<div class="sxn-testimonials">
	<div class="testsWrap">
		<div class="testsList">
			<div class="test">
				<div class="testWrap">
					<div class="testQuotes"></div>
					<p class="testBody">I retained Orlando Legal for a contested divorce, after the first firm acquired failed in every aspect of the word.  Jonathan Simon and Jennifer Santana were patient , considerate and compassionate to my cause and position. I only wanted fair resolution and continue to be a father to my children. This was my motto when I started and the same when I finished. However opposing party had other intentions. From the beginning Jonathan provided a consultation and assessed my concerns and gave me an answer from the start...</p>
					<span class="testAuthor">~ Mario R. ~</span>
				</div>
			</div>
			<div class="test">
				<div class="testWrap">
					<div class="testQuotes"></div>
					<p class="testBody">where would my family be without them.... i am a single father i have been fighting to stay in my sons  life since he was born in 2011.... in the beginning i did all the leg work filed my papers etc etc well to make a long story short about a year or so ago my sons mom  ran to another state took my son... i was referred to them through my family church not only did i get to go to ohio to get my son but i have full custody of my son. you want a firm that cares about family? you want a firm that will fight? stop looking i would never use anyone else ever i will spend the rest of my life with my son because of them</p>
					<span class="testAuthor">~ Elwin Raynor ~</span>
				</div>
			</div>
			<div class="test">
				<div class="testWrap">
					<div class="testQuotes"></div>
					<p class="testBody">Jonathan and his team members performed an excellent service in my case! I was quite skeptical when my fiancée first mentioned about Jonathan and his firm. I had a horrible attorney at that time; besides, I was very distraught due to the situation my son’s mother created. After one-hour consultation with Jonathan, I knew that he was the right person to represent me! He was patient yet professional – He described the procedures and possible outcomes in detail; he also explained his strategy in depth, and that gesture put me in ease.</p>
					<span class="testAuthor">~ Nick & B ~</span>
				</div>
			</div>
			<div class="test">
				<div class="testWrap">
					<div class="testQuotes"></div>
					<p class="testBody">I felt supported, I felt safe, I felt in the end, it would all be okay. Not only did Jonathan have my back so to back to speak, but he and his wife practicing as a dynamic duo became people that I now call friends. I wish for no one to endure divorce, let alone with children... but if its an inevitable necessity, I cannot and would not recommend anyone else... because much like life through divorce, no one should have to settle in life... and that should go for your representation as well.</p>
					<span class="testAuthor">~ June Townsend ~</span>
				</div>
			</div>
			<div class="test">
				<div class="testWrap">
					<div class="testQuotes"></div>
					<p class="testBody">Simply the Best.  Jonathan Simon and the Orlando Family Firm should be your first and only choice.  For the past 5 years, Jonathan has guided and coached our family through some extremely challenging situations.  He has protected my children on numerous occasions in court and out of court.  He consistently looks for peaceful resolve out of court, but can more than handle himself if court in necessary.  "WOW" is all I can say about Jonathan's skill set in the court room.  He will fight for you and your children to the end...</p>
					<span class="testAuthor">~ Cheryl Ventura ~</span>
				</div>
			</div>
			<div class="test">
				<div class="testWrap">
					<div class="testQuotes"></div>
					<p class="testBody">I recently divorced after 18 years of marriage and two children, although; one is in college. I chose Orlando Family Law Firm to represent me and was not disappointed. Jonathan Simon represented me, and advocated for my interests with skill and finess. I was pleased with the settlement I was awarded. His entire office staff is professional, personal, and helpful in guiding one through the legal process of filings and discovery. I highly recommend Orlando Family Law Firm, as Mr. Simon is an expert in Family Law, who can navigate the legal system...</p>
					<span class="testAuthor">~ Lynn Paoli ~</span>
				</div>
			</div>
			<div class="test">
				<div class="testWrap">
					<div class="testQuotes"></div>
					<p class="testBody">When I met Mr. Simon I knew right away that he was the one. He took the time to explain everything about the laws and what could happen. I decided to hire him. As he worked through my case he also had his team from Orlando Legal to assist. I had made a good decision to hire Jonathon and the Orlando Legal firm. Everyone was amazing and courteous. As Jonathon worked on my case the outcome came to better than I thought. I got to know Jonathon a little bit during the painful moments of my life. Not only his work ethic and values were astonishing, but he is genuine good man...</p>
					<span class="testAuthor">~ Felix Lopez ~</span>
				</div>
			</div>
			<div class="test">
				<div class="testWrap">
					<div class="testQuotes"></div>
					<p class="testBody">I have been represented several times by both Jonathan Simon and Cynthia Winter. They are both compassionate, brilliant and great communicators. The office is well run, staffed with friendly, helpful employees. I recommend Orlando Legal to anyone needing assertive, creative, well planned representation.</p>
					<span class="testAuthor">~ Nikki Vernon ~</span>
				</div>
			</div>
			<div class="test">
				<div class="testWrap">
					<div class="testQuotes"></div>
					<p class="testBody">Highly experienced, courteous professionals! Cynthia Winter and my attorney, Eric Bensen, were always there for me from the very start. Highly recommend Orlando Legal for anyone needing legal representation!</p>
					<span class="testAuthor">~ Micki Francis Noto ~</span>
				</div>
			</div>
			<div class="test">
				<div class="testWrap">
					<div class="testQuotes"></div>
					<p class="testBody">In an industry where reputation is everything, Attorney Simon and his team are very organized and well run with all of the necessary resources to get the job done. I highly recommend them!</p>
					<span class="testAuthor">~ Rob Bamberg ~</span>
				</div>
			</div>
		</div> <!-- end testsList -->
		<ul class="testsArrows">
			<li class="taPrev"><a><img src="img/testimonials/arrows.png" alt="Previous" title="Previous"></a></li>
			<li class="taNext"><a><img src="img/testimonials/arrows.png" alt="Next" title="Next"></a></li>
		</ul>
	</div>
</div><!-- end sxn-testimonials -->
<footer id="mainFooter">
	<div class="mfWrap">
		<div class="mfLeft">
			<div class="mfNumbers">
				<span>Phone <a href="tel:+14073776399">407-377-6399</a></span>
				<span>Fax <a>407-377-6699</a></span>
			</div>
			<span class="mfHours">Office Hours:<br> 8:30-5 (M-F)</span>
			<span class="mfAddress">800 N. Magnolia Ave., Suite 1500<br>
			Orlando, FL 32803</span>
		</div> <!-- end mfLeft -->
		<div class="mfMiddle">
			<span class="mfTitle">Same Day Consultations</span>
			<span class="mfSubtitle">After Regular Business Hours<br> Consultations Available</span>
		</div> <!-- end mfMiddle -->
		<div class="mfRight">
			<ul class="fNav" id="fNav1">
			<?php wp_nav_menu(array('theme_location' => 'footer-nav', 'container' => false, 'items_wrap' => '%3$s')); ?>
			</ul>	
			<div id="mmBrandingWrapper">
				<div id="mmBranding">
					<img id="logo-mmm" src="img/logos/mmm.png" alt="MileMark Media - Practice Growth Solutions" title="MileMark Media - Practice Growth Solutions">
					<p>&copy; 2018 Greater Orlando Family Law. All rights reserved. <br>This law firm website is managed by <a target="_blank" href="http://www.milemarkmedia.com/">MileMark Media</a>.</p>
				</div><!-- #mmBranding -->
			</div><!-- #mmBrandingWrapper -->		
		</div> <!-- end mfRight -->
	</div> <!-- end mfWrap -->
</footer>
</div><!-- wrapper -->
<!-- END COPY -->













<div id="sync">
	<span id="syncMenu" class="syncItem"></span>
</div>

<script>
//Set body and nav class to desktop ones as necessary
(function(){
	var syncMenu = document.getElementById('syncMenu');
	if(syncMenu === null || !document.defaultView || !document.defaultView.getComputedStyle) {return false;}	
	var smZindex = document.defaultView.getComputedStyle(syncMenu, null).getPropertyValue('z-index');
	if(smZindex > 0) {
		var b = document.getElementsByTagName('body')[0];
		var menu = document.getElementById('mainMenu');
		b.classList.remove('mob_menu');
		b.classList.add('desktop_menu');
		menu.classList.remove('mm_mob');
		menu.classList.add('mm');
	}
})()
</script>

<script><?php include(get_stylesheet_directory() . '/js/head.min.js') ?></script>
<script>head.js({ jQuery: "https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" });
head.ready("jQuery", function() {head.load("https://d78c52a599aaa8c95ebc-9d8e71b4cb418bfe1b178f82d9996947.ssl.cf1.rackcdn.com/dsk_slides/dsk_slides.1.0.2.min.js", "//ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js","<?php echo get_stylesheet_directory_uri(); ?>/js/slick.min.js", function() {head.load("<?php echo get_stylesheet_directory_uri(); ?>/js/<?php echo DSKdotMinCheck('script.js'); ?>?t=<?= time() ?>")});});</script>

<?php wp_footer(); ?>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
 	"@type": "Attorney",
	"name": "FIRM_NAME_HERE",
	"image" : "<?php echo get_stylesheet_directory_uri(); ?>/screenshot.png",
	"address":
	[
    	{
			"@type": "PostalAddress",
			"streetAddress": "STREET_ADDRESS_HERE",
			"addressLocality": "CITY_HERE",
			"addressRegion": "STATE_ABBREVIATION_HERE",
			"postalCode": "ZIP_CODE_HERE",
			"telephone": "xxx-xxx-xxxx",
			"faxNumber": "xxx-xxx-xxxx"
    	}
	]
}
</script>
</body></html>