(function ($) {
	var htmlEL = $('html'),
	bodyEL = $('body'),	
	htmlAndBody = $('html, body'),
	isFrontPage = bodyEL.hasClass('home'),
	rTO,
	mainMenu = $('#mainMenu'),
	syncMenu = $('#syncMenu'),
	fsMenuOn = false,
	lastPos,
	menuToggle = $('.menuToggle');	
	
	//Form JS Begins
	$(".validateForm").validate();
	//Clear Inputs / Textareas that are not of the type 'submit'
	$('input,textarea').each(function() {if($(this).attr('type')!= 'submit'){var default_value = this.value;$(this).focus(function() {if(this.value == default_value) {this.value = '';}});$(this).blur(function() {if(this.value == '') {this.value = default_value;}});}$(this).hover(function(){$(this).addClass('display');},function(){$(this).removeClass('display');});});
	//Form JS Ends

	/*
	if( isFrontPage ) {
		$('#homeslides').dsk_slides({
			slideClass : 'homeslide',
			effect : 'fade',
			prevClass : 'prevSlide',
			nextClass : 'nextSlide',
			pause: 7000,
			speed: 400,
            onInit: function() {
                setTimeout(function() {
                    $('#homeslide1').addClass('slideTxtAnimate');
                }, 400);
            },
            onStart: function() {
                setTimeout(function() {
                    $('.incomingSlide').addClass('slideTxtAnimate');
                }, 400);
            },
            onComplete: function() {
                setTimeout(function() {
                    $('.outgoingSlide').removeClass('slideTxtAnimate');
                }, 400);
            }
		});
	}
	*/









	// BEGIN COPY

	//Homepage Team Zone
	var team = [
		{
			"name" : "Eric Lee Bensen, Esq.",
			"body" : "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.</p>",
			"class" : "bensen",
			"link" : "#"
		},
		{
			"name" : "Kenneth J. Cotter, Esq.",
			"body" : "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim.</p>",
			"class" : "cotter",
			"link" : "#"
		},
		{
			"name" : "Laura A. Moffett, Esq.",
			"body" : "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.</p>",
			"class" : "moffett",
			"link" : "#"
		},
		{
			"name" : "Lorna M. Truett, Esq.",
			"body" : "<p>Led do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.</p>",
			"class" : "truett",
			"link" : "#"
		},
		{
			"name" : "Cynthia M. Winter, Esq.",
			"body" : "<p>Lorem ipsum dolor sit amet et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.</p>",
			"class" : "winter",
			"link" : "#"
		},
		{
			"name" : "Michael J. Vaghaiwalla, Esq.",
			"body" : "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>",
			"class" : "vaghaiwalla",
			"link" : "#"
		}
	]
	var currAtt;
	$('.teamMem').click(function(e){
		e.preventDefault();
		//Change displayed info
		currAtt = $(this).parent().children().index(this);
		$('.memBox').attr('class', 'memBox ' + team[currAtt].class);
		$('.memName').text(team[currAtt].name);
		$('.memBody').html(team[currAtt].body);
		$('.memMore').attr('href', team[currAtt].link);
		//Set Att Btn Style
		$('.active').removeClass('active');
		$(this).addClass('active');
	})


	$('.testsList').slick({
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow: $('.taPrev'),
		nextArrow: $('.taNext'),
		//  MOD
		responsive: [
			{
				breakpoint: 1120,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	// END COPY









	function bodyMenuClassToggle(goMob) {
		if(goMob) {bodyEL.removeClass('dsktp_menu').addClass('mob_menu');}
		else {bodyEL.removeClass('mob_menu').addClass('dsktp_menu');}
	}
	function mmToggle(goMob) {
		if(goMob) {mainMenu.removeClass('mm').addClass('mm_mob');} 
		else {mainMenu.removeClass('mm_mob').addClass('mm');}
	}
	function menuer() {
		if(syncMenu.css('z-index') == 1) {
			bodyMenuClassToggle(false);
			if(!fsMenuOn) {mmToggle(false);}
		} else {
			bodyMenuClassToggle(true);
			if(!fsMenuOn) {mmToggle(true);}
		}
	}
	function fullScreenMenu(enableFS) {
		if(enableFS) {
        	fsMenuOn = true;
			lastPos = bodyEL.scrollTop();
			if(lastPos == 0) {lastPos = htmlEL.scrollTop();}
	        bodyEL.addClass('fsMenuOn');
	        htmlAndBody.animate({scrollTop: 0}, 0);
    	    mmToggle(true);
		} else {
        	fsMenuOn = false;
	        bodyEL.removeClass('fsMenuOn');
	        htmlAndBody.animate({scrollTop: lastPos}, 0);
        	menuer();
		}
	}	
    menuToggle.click(function(e){
        e.preventDefault();
        if(!fsMenuOn) {fullScreenMenu(true);} 
        else {fullScreenMenu(false);}
    });

	var stickyZone = false,
	isTouchDevice = 'ontouchstart' in window || 'onmsgesturechange' in window;
	function stickyMenu() {
		if( $(window).scrollTop() > 50 ) {
			if(stickyZone) {return;}			
			bodyEL.addClass('sticky_zone');
			stickyZone = true;
		} else {
			if(!stickyZone) {return;}
			bodyEL.removeClass('sticky_zone');
			stickyZone = false;
		}
	}
    	
	/* Init */
	menuer();
//	if(isTouchDevice) {animation_elements.removeClass('animated');}
	stickyMenu();	
	
	/* Resize */
	$(window).resize(function(){
		rTO = setTimeout(function(){
			clearTimeout(rTO);
			menuer();
			stickyMenu();
		}, 100);
	});
	
	/* Scroll */
	$(window).on({'scroll touchmove': function(){	
		stickyMenu();		
	}});
	
	/* Target _blank Issue */
	(function(){
		$('a[target=_blank]').each(function(){
			var newREL = $(this).attr('rel');
			if(newREL === undefined) {newREL = "";} 
			else {newREL += " ";}
			newREL += "noopener noreferrer";		
			$(this).attr('rel',  newREL)
		});
	})();
	//Allow dropdowns on touchscreens
	(function(){var touchedScreen = false;
	window.addEventListener('touchstart', function() {
	  touchedScreen = true;
	});	
	$('nav.mm a').click(function(e){			
		if(!touchedScreen) {return true;}
		var clickedItem = $(this);			
		if(clickedItem.siblings('ul').length === 0) {
			return true;
		}
		var clickedAttr = clickedItem.attr('data-clicked');
		if(typeof clickedAttr === typeof undefined) {				
			e.preventDefault();
			clickedItem.attr('data-clicked', '1');
		}
	});})();
	//Sharing icons
	(function(){
		$('a.dskSharingIcon').click(function(){
			var url = $(this).data('url');
			$(this).attr('href', url);
		});
	})();
}(jQuery));
(function(){var head=document.getElementsByTagName("head")[0],fonts=document.createElement("link");fonts.rel="stylesheet",fonts.type="text/css",fonts.href="https://fonts.googleapis.com/css?family=Lato:400,700|Merriweather:400,400i",fonts.media="all",head.appendChild(fonts);
})();